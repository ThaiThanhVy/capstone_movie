import { localServ } from "../../Services/LocalService"
import { SET_USER } from "../Constants/ConstantsUser"


const initialState = {
    userInfor: localServ.user.get()
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER: {
            state.userInfor = action.payload
        }
        default: return state
    }
}

export default userReducer;