import { DETAIL_MOVIE } from "../Constants/ManageTheatre"



const stateDefault = {
    detailMovie: {}
}

const reducerManageMovie = (state = stateDefault, action) => {
    switch (action.type) {

        case DETAIL_MOVIE: {
            state.detailMovie = action.detailMovie

            return { ...state }
        }

        default: return { ...state }
    }
}

export default reducerManageMovie; 