
import { quanLyDatVeServ } from "../../Services/BookticketsManage";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { DETAIL_MOVIE } from "../Constants/ManageTheatre";
import { DAT_VE, LAY_CHI_TIET_PHONG_VE } from "../Constants/QuanLiDatVe";


export const quanLiDatVeAction = (maLichChieu) => {
    return async dispatch => {
        try {
            const result = await quanLyDatVeServ.layChiTietPhongVe(maLichChieu);
            console.log('result: ', result);

            dispatch({
                type: LAY_CHI_TIET_PHONG_VE,
                chiTietPhongVe: result.data.content
            })
        } catch (err) {
            console.log('err: ', err.response?.data);
        }
    }
}

export const datVeAction = (thongTinDatVe = new ThongTinDatVe) => {
    return async dispatch => {
        try {
            const result = await quanLyDatVeServ.datVe(thongTinDatVe);
            console.log('result: ', result.data.content);
        } catch (err) {
            console.log('err: ', err);
        }
    }
}