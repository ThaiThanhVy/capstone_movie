import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css';
import Layout from './Layout/Layout';
// npm i ant design
import "antd/dist/antd.css";
import DetailPage from './Page/DetailPage/DetailPage';
import HomePage from './Page/HomePage/HomePage';
import LoginPage from './Page/LoginPage/LoginPage';
import Contact from './Page/Contact/Contact';
import Spinner from './components/Spinner/Spinner';
import Checkout from './Page/Checkout/Checkout';
import DashBoard from './Page/Admin/DashBoard/DashBoard';

function App() {


  return (
    <div>
      <Spinner />
      {/* BrowserRouter Giống như Provider có cái này thì Routes mới hoạt động */}
      <BrowserRouter>
        {/* Phân Trang */}
        <Routes>
          <Route
            path="/"
            element={
              <Layout Component={HomePage} />
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/contact" element={<Contact />} />
          <Route path="/checkout/:id" element={<Checkout />} />
          <Route path="/dashboard" element={<DashBoard />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
