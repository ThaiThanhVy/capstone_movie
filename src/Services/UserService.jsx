import axios from "axios"
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./ConfigURL"

export const movieServ = {
    postLogin: (data) => {
        // console.log('data: ', data);
        // return axios({
        //     url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
        //     // POSt lên api để người dùng nhập vào tài khoảng mật khẩu
        //     // POST đẩy dữ liệu lên 
        //     method: "POST",
        //     // Khi đó back end sẽ trả về cái data
        //     data,
        //     // Bổ sung thông tin cho backEnd biết
        //     headers: {
        //         TokenCybersoft: TOKEN_CYBERSOFT,
        //     }
        // });

        let uri = `/api/QuanLyNguoiDung/DangNhap`
        return https.post(uri, data)
    },
};