import axios from "axios"
import { BASE_URL, TOKEN_CYBERSOFT } from "./ConfigURL"

export const movieServ = {
    getListMovie: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03`,
            method: "GET",
            // Bổ sung thông tin cho backEnd biết
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            }
        });
    },
    getDataMovieBytheater: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03`,
            method: "GET",
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            }
        })
    },

    getDataCarousel: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
            method: "GET",
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            }
        })
    }
};