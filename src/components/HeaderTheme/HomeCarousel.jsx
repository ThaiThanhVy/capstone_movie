import React from 'react'
import { Carousel } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import axios from 'axios';
import { BASE_URL } from '../../Services/ConfigURL';
import { movieServ } from '../../Services/CarouselService';
import { useState } from 'react';

export default function HomeCarousel(props) {
    const { arrImgCarousel } = useSelector(state => state.reducerCarousel)
    console.log('arrCarousel: ', arrImgCarousel);
    // useEffect(async () => {
    //     try {
    //         const result = await axios({
    //             url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner`,
    //             method: 'GET',
    //         })
    //     } catch (err) {
    //         console.log('err', err)
    //     }

    // }, [])

    let dispatch = useDispatch([])

    useEffect(() => {
        movieServ.getItemBannerCarousel()
            .then((res) => {
                console.log('carousel', res)
                dispatch({
                    type: 'DISPATCH_CAROUSEL',
                    arrImgCarousel: res.data.content,
                })
            })
            .catch((err) => {
                console.log(err)
            })
    }, [])



    const renderImgCarousel = () => {
        return arrImgCarousel.map((item, index) => {
            const contentStyle = {
                height: '600px',
                color: '#fff',
                lineHeight: '160px',
                textAlign: 'center',
                background: '#364d79',
            };
            return (
                <div key={index}>
                    <h3 style={contentStyle}><img className="w-full h-full" src={item.hinhAnh} alt />
                    </h3>
                </div>
            )
        })
    }

    return (
        <Carousel autoplay>
            {renderImgCarousel()}
        </Carousel>
    )
}
