import React from 'react'
import { NavLink } from 'react-router-dom'
import { USER } from '../../Services/LocalService'
import UserNav from './UserNav'
export default function HeaderTheme() {


    if (localStorage.getItem(USER)) {
        return <div>
            {/* <span className='text-amber-400 italic font-medium text-3xl animate-bounce'>CyberMovie</span> */}
            <header style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }} className="p-4 text-gray-100 fixed w-full z-10">
                <div className=" bg-opacity-40 flex justify-between h-16 mx-auto">
                    <a rel="noopener noreferrer" href="#" aria-label="Back to homepage" className="flex items-center p-2 animate-bounce">
                        <NavLink to={'/'}><h3 className="w-8 h-8 text-violet-400 text-3xl">CyberMovie</h3></NavLink>
                    </a>
                    <ul className="items-stretch hidden space-x-3 lg:flex">
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#Card" className=" flex items-center px-4 -mb-1 border-b-2 border-transparent text-white border-violet-400">Lịch Chiếu</a>
                        </li>
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#tabsmovies" className="flex items-center px-4 -mb-1 border-b-2 border-transparent text-white">Cụm Rạp</a>
                        </li>
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#" className="flex items-center px-4 -mb-1 border-b-2 border-transparent text-white">Tin Tức</a>
                        </li>
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#footer" className="flex items-center px-4 -mb-1 border-b-2 border-transparent text-white">Ứng Dụng</a>
                        </li>
                        <li className="flex">
                            <NavLink to={"/dashboard"}>
                                <a style={{ paddingTop: 16 }} rel="noopener noreferrer" href="#footer" className="flex items-center justify-center px-4 -mb-1 border-b-2 border-transparent text-white">Quản Lí</a>
                            </NavLink>
                        </li>
                    </ul>
                    <div className="items-center flex-shrink-0 hidden lg:flex">
                        <UserNav />

                    </div>
                    <button className="p-4 lg:hidden">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-6 h-6 text-gray-100">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </button>
                </div>
            </header>
        </div>
    }


    return (
        // from MANBA UI
        <div>
            {/* <span className='text-amber-400 italic font-medium text-3xl animate-bounce'>CyberMovie</span> */}
            <header style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }} className="p-4 text-gray-100 fixed w-full z-10">
                <div className=" bg-opacity-40 flex justify-between h-16 mx-auto">
                    <a rel="noopener noreferrer" href="#" aria-label="Back to homepage" className="flex items-center p-2 animate-bounce">
                        <NavLink to={'/'}><h3 className="w-8 h-8 text-violet-400 text-3xl">CyberMovie</h3></NavLink>
                    </a>
                    <ul className="items-stretch hidden space-x-3 lg:flex">
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#Card" className=" flex items-center px-4 -mb-1 border-b-2 border-transparent text-white border-violet-400">Lịch Chiếu</a>
                        </li>
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#tabsmovies" className="flex items-center px-4 -mb-1 border-b-2 border-transparent text-white">Cụm Rạp</a>
                        </li>
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#" className="flex items-center px-4 -mb-1 border-b-2 border-transparent text-white">Tin Tức</a>
                        </li>
                        <li className="flex">
                            <a rel="noopener noreferrer" href="#footer" className="flex items-center px-4 -mb-1 border-b-2 border-transparent text-white">Ứng Dụng</a>
                        </li>
                    </ul>
                    <div className="items-center flex-shrink-0 hidden lg:flex">
                        <UserNav />

                    </div>
                    <button className="p-4 lg:hidden">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-6 h-6 text-gray-100">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </button>
                </div>
            </header>
        </div>
    )
}
