import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import HomeCarousel from '../../components/HeaderTheme/HomeCarousel'
import Spinner from '../../components/Spinner/Spinner'
import { setLoadingOffAction, setLoadingOnAction } from '../../Redux/Actions/ActionSpinner'
import { movieServ } from '../../Services/MovieService'
import ItemMovies from './ItemMovies'
import TabsMovies from './TabsMovies'


export default function HomePage() {

    // tạo state và setState bằng useState
    const [movies, setMovies] = useState([])

    const dispatch = useDispatch()
    // setIsloading

    // const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        // setIsLoading(true)
        dispatch(setLoadingOnAction())
        movieServ.getListMovie()
            .then((res) => {
                console.log(res)
                // setIsLoading(false)
                dispatch(setLoadingOffAction())
                setMovies(res.data.content);
            })
            .catch((err) => {
                // setIsLoading(false)
                dispatch(setLoadingOffAction())
                console.log(err)
            })
    }, [])

    const renderMovies = () => {
        return movies.map((data, index) => {
            return <ItemMovies key={index} data={data} />
        })
    }


    return (

        <div>
            <HomeCarousel />
            <br />
            <br />
            <br />
            <div className='grid grid-cols-5 gap-10 container mb-5'>
                {renderMovies()}

                {/* Ghi chữ hello ở đây thì nó sẽ chạy không được */}
                {/* Nằm chữ Hello nó là childer */}
                {/* Nên muốn nó chạy thì phải thêm childer vào cái file muốn lấy và cho cái props data = {} */}
                {/* VD */}
                {/* <ItemMovies>Ví Dụ Children</ItemMovies> */}
            </div>
            <div className='container' id='tabsmovies'>
                <TabsMovies />
            </div>
            {/* {isLoading && <Spinner />} */}
        </div>
    )
}
