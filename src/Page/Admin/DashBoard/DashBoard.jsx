import {
    DesktopOutlined,
    FileOutlined,
    PieChartOutlined,
    TeamOutlined,
    UserOutlined,
} from '@ant-design/icons';
import { Breadcrumb, Layout, Menu } from 'antd';
import React, { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom'
import { useSelector } from 'react-redux';
import { localServ, USER } from '../../../Services/LocalService';
import UserNav from '../../../components/HeaderTheme/UserNav';
const { Header, Content, Footer, Sider } = Layout;


function getItem(label, key, icon, children) {
    return {
        key,
        icon,
        children,
        label,
    };
}

const items = [
    getItem('Option 1', '1', <PieChartOutlined />),
    getItem('Option 2', '2', <DesktopOutlined />),
    getItem('User', 'sub1', <UserOutlined />, [
        getItem('Tom', '3'),
        getItem('Bill', '4'),
        getItem('Alex', '5'),
    ]),
    getItem('Team', 'sub2', <TeamOutlined />, [getItem('Team 1', '6'), getItem('Team 2', '8')]),
    getItem('Files', '9', <FileOutlined />),
];

const DashBoard = () => {
    let navigate = useNavigate()
    let user = useSelector((state) => {
        return state.userReducer.userInfor
    })
    const [collapsed, setCollapsed] = useState(false);

    if (!localStorage.getItem(USER)) {
        alert('Bạn Không có quyền truy Cập vào trang này')
        return window.location.href = "/"
    }

    if (user.maLoaiKhachHang !== 'QuanTri') {
        alert('Ban ko cos Uywn')
        return window.location.href = "/"
    }

    let handleRemove = () => {
        localServ.user.remove()
        setTimeout(() => {
            window.location.href = "/"
        }, 1000)
    }

    let opaerations = () => {
        if (user) {
            return <>
                <button style={{ width: 52, height: 52, backgroundColor: 'rgba(191, 62, 129, 0.8)' }} className=' mr-3 font-medium rounded-full opacity-100'>{user.hoTen.substr(0, 1)}</button>
                <button onClick={() => { handleRemove() }} className='self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 dark:text-gray-900'>
                    Đăng Xuất
                </button>
                {/* border  border-none hover:scale-110 hover:duration-500  rounded px-5 py-2 text-light bg-green-500 */}
            </>
        }
    }
    return (
        <Layout
            style={{
                minHeight: '100vh',
            }}
        >
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div className="logo" />
                <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
            </Sider>
            <Layout className="site-layout">
                <Header
                    className="site-layout-background"
                    style={{
                        padding: 0,
                    }}
                />
                <Content
                    style={{
                        margin: '0 16px',
                    }}
                >
                    <Breadcrumb
                        style={{
                            margin: '16px 0',
                        }}
                    >
                        <Breadcrumb.Item>User</Breadcrumb.Item>
                        <Breadcrumb.Item>Bill</Breadcrumb.Item>
                    </Breadcrumb>
                    <div
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            minHeight: 360,
                        }}
                    >
                        Bill is a cat.
                    </div>
                </Content>
                <Footer
                    style={{
                        textAlign: 'center',
                    }}
                >
                    Ant Design ©2018 Created by Ant UED
                </Footer>
            </Layout>
        </Layout>
    );
};

export default DashBoard;