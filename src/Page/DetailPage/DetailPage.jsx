import { Tabs } from 'antd'
import axios from 'axios'
import moment from 'moment'
import React from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NavLink, useParams } from 'react-router-dom'
import '../../assets/styles/circleRatting.scss'
import { ManageDetailMovie } from '../../Redux/Actions/ManageTheatre'
import { Rate } from 'antd';
import TabPane from 'antd/lib/tabs/TabPane'



export default function DetailPage(props) {


    let img = {
        height: '314px',
        display: ' block',
        position: 'relative',
        borderRadius: '4px',
        backgroundSize: 'cover',
        backgroundRepeat: ' no-repeat',
        backgroundPosition: 'center',
    }

    let ratting = {
        marginLeft: 200,
        marginTop: 20,
        fontSize: 50,
    }

    let { detailMovie } = useSelector(state => state.reducerManageMovie)

    console.log('detailMovie: ', detailMovie);

    let dispatch = useDispatch()



    let { id } = useParams();
    console.log('id: ', id);

    useEffect(() => {
        dispatch(ManageDetailMovie(id))
    }, [])



    return (
        <div className='bg-gray-800 pb-5' style={{ minHeight: '100vh' }} >
            <div className='grid grid-cols-12 pt-5' style={{ marginRight: 150 }}>
                <div className='grid grid-cols-2 cols-span-4 col-start-4 pt-40' style={{ width: 850 }}>
                    <div className='grid grid-cols-2'>
                        <img style={img} src={detailMovie.hinhAnh} alt="" />
                        <div className='ml-4'>
                            <p className='text-red-400 text-2xl font-serif'>{detailMovie.tenPhim}</p>
                            <p className='text-white p-3 rounded bg-red-600'>{moment(detailMovie.ngayKhoiChieu).format("DD-MM-YYYY ~ hh:mm")}</p>
                            <p className='text-white w-80'>{detailMovie.moTa}</p>
                            <p className='text-white'>{detailMovie.thoiLuong}</p>
                            <a href="#muaVe" className='btn btn-danger'>Mua Ve</a>
                        </div>
                        <br />
                    </div>
                    <div className='grid grid-cols-4' style={ratting}>
                        <div className="pacss-wrapper">
                            <span className="pacss-foreground">
                                <span className="pacss-number text-xs mt-3">{detailMovie.danhGia * 10}%</span>
                            </span>
                            <span className="pacss pacss-100 pacss-big" />
                            <h1 style={{ marginTop: -112, marginLeft: 11 }}>
                                <Rate allowHalf value={detailMovie.danhGia / 2} />
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div className='w-2/3 container px-5 py-2 bg-white '>
                <Tabs className='flex justify-center' defaultActiveKey="1">
                    <Tabs.TabPane tab={<h1 id='muaVe' className='text-dark'>Lịch Chiếu</h1>} key="1">
                        <div style={{ overflowY: "scroll" }} className='flex flex-row justify-center justify-items-center container scrollbar scrollbar-thumb-blue-700 scrollbar-track-blue-300 overflow-y-scroll hover:scrollbar-thumb-green-700'>
                            <Tabs className='duration-500 w-full h-full' style={{ height: 500 }} tabPosition="left">
                                {detailMovie.heThongRapChieu?.map((htr, index) => {
                                    return <Tabs.TabPane tab={<div><img className='w-16 h-16' src={htr.logo} alt="" />
                                    </div>} key={index}>
                                        {htr.cumRapChieu?.map((cumRap, index) => {
                                            return <div key={index} className='w-48 text-left'>
                                                <p className='text-green-700 truncate font-bold'>
                                                    {cumRap.tenCumRap}
                                                </p>
                                                <p className='truncate text-blue-500'>
                                                    {cumRap.diaChi}
                                                </p>

                                                {cumRap.lichChieuPhim?.map((lichChieu, index) => {
                                                    return <div key={index} className='w-48 text-left'>
                                                        <NavLink to={`/checkout/${lichChieu.maLichChieu}`}>
                                                            <div className='flex-grow'>
                                                                <p className='p-3 rounded bg-red-600 text-white'>
                                                                    {moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY ~ hh:mm")}
                                                                </p>
                                                            </div>
                                                        </NavLink>
                                                    </div>
                                                })}
                                            </div>
                                        })}
                                    </Tabs.TabPane>
                                })}
                            </Tabs>
                        </div>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab={<h1 className='text-dark'>Thông Tin</h1>} key="2">
                        Thông Tin
                    </Tabs.TabPane>
                    <Tabs.TabPane tab={<h1 className='text-dark'>Đánh Giá</h1>} key="3">
                        Đánh Giá
                    </Tabs.TabPane>
                </Tabs>
            </div>
        </div >
    )
}
