import React, { Fragment } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { USER_LOGIN } from '../../Redux/Constants/UserLogin'
// import { SET_USER } from '../../Redux/Constants/ConstantsUser'
import '../../assets/styles/screen.css'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { datVeAction, quanLiDatVeAction } from '../../Redux/Actions/QuanLiDatVeAction'
import { CloseOutlined } from '@ant-design/icons'
import { DAT_VE } from '../../Redux/Constants/QuanLiDatVe'
import _ from 'lodash'
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe"


export default function Checkout(props) {

    const { chiTietPhongVe, danhSachGheDangDat } = useSelector(state => state.quanLiDatVeReducer)
    console.log('danhSachGheDangDat: ', danhSachGheDangDat);
    console.log('chiTietPhongVe: ', chiTietPhongVe);

    let dispatch = useDispatch()

    // if (JSON.parse(jsonData) == null) {
    //     window.location.href = "/login"
    // }

    let { id } = useParams()
    console.log('id: ', id);

    useEffect(() => {
        dispatch(quanLiDatVeAction(id))
    }, [])

    let { thongTinPhim, danhSachGhe } = chiTietPhongVe;

    const renderDanhSachGhe = () => {
        return danhSachGhe?.map((ghe, index) => {

            let gheVip = ghe.loaiGhe === 'Vip' ? 'gheVip' : '';
            let gheDaDat = ghe.daDat === true ? 'gheDuocChon' : '';

            let gheDangDat = '';

            let indexGheDangDat = danhSachGheDangDat.findIndex(gheDangDat => gheDangDat.maGhe === ghe.maGhe)

            if (indexGheDangDat !== -1) {
                gheDangDat = 'gheDangChon'
            }

            return <Fragment key={index}>
                <button onClick={() => {
                    dispatch({
                        type: DAT_VE,
                        ghe
                    })
                }} disabled={ghe.daDat} className={`ghe ${gheVip} ${gheDaDat} ${gheDangDat} text-center`} key={index}>

                    {ghe.daDat ? <CloseOutlined style={{ verticalAlign: 'middle' }} /> : ghe.stt}
                </button>

                {(index + 1) % 16 === 0 ? <br /> : ''}
            </Fragment >
        })
    }


    return (
        <div className=' bg-gray-800'>
            <div className='grid grid-cols-12'>
                <div className='col-span-8 min-h-screen'>
                    <div className='screen'></div>
                    <div className='text-white text-center'>Man hinh</div>
                    <div className='mt-2 text-center'>
                        {renderDanhSachGhe()}
                    </div>
                    <div className='flex justify-center items-center'>
                        <div className='ghe gheVip'></div>
                        <br />
                        <span className='text-white'>Ghế Vip</span>
                        <div className='ghe gheDuocChon'></div>
                        <br />
                        <span className='text-white'>Ghế Đã Đặt</span>
                        <div className='ghe gheDangChon'></div>
                        <br />
                        <span className='text-white'>Ghế Đang Chọn</span>
                    </div>
                </div>
                <div className='col-span-4 bg-white h-100'>
                    <h3 className='text-center text-2xl text-green-400 mt-4 mb-4'>{
                        danhSachGheDangDat.reduce((tongTien, ghe, index) => {
                            return tongTien += ghe.giaVe
                        }, 0).toLocaleString()
                    } VND</h3>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Cụm Rạp:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.tenCumRap}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Địa chỉ:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.diaChi}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Rạp:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.tenCumRap}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Ngày giờ chiếu:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.ngayChieu} ~ {thongTinPhim?.gioChieu}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Tên Phim:</h3>
                        <h3 className='text-green-500 text-base font-bold text-right'>{thongTinPhim?.tenPhim}</h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Chọn Ghế:</h3>
                        <h3>
                            {_.sortBy(danhSachGheDangDat, ['stt'])?.map((gheDangDat, index) => {
                                return <span key={index} className='ml-2 text-green-500 text-base font-bold'>
                                    <span> {gheDangDat.stt}</span>
                                </span>
                            })}
                        </h3>
                    </div>
                    <hr />
                    <div className='flex justify-between mx-3 mb-3 mt-3'>
                        <h3 className='text-black text-base font-bold'>Hình Ảnh</h3>
                        <img className='text-green-500 text-base font-bold text-right w-24 h-full mr-4' src={thongTinPhim?.hinhAnh} alt="" />
                    </div>
                    <hr />
                    <div className='flex justify-center mb-0 w-full h-16 bg-orange-600 items-center'>
                        <div onClick={() => {
                            let thongTinDatVe = new ThongTinDatVe
                            thongTinDatVe.danhSachVe = danhSachGheDangDat
                            thongTinDatVe.maLichChieu = thongTinPhim.maLichChieu
                            console.log('thongTinDatVe: ', thongTinDatVe)
                            dispatch(datVeAction(thongTinDatVe))
                        }} className='text-light text-3xl cursor-pointer'>
                            Đặt Vé
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
